# This is solution for wap's prior examination written by hanson yu.

problem1: 
implement two solution:
one is time: O(n), Space is O(1440)
the other is make use of segment, O(n*log5000).

problem2:
Time: O(n+1440)
Space: O(n)

PersistentQueue:
enqueue: O(1)
dequeue: O(n)
peek: O(1)
size: O(1)
