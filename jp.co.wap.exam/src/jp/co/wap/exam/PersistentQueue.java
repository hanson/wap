/**
 * 
 */
package jp.co.wap.exam;

import java.util.NoSuchElementException;
import jp.co.wap.exam.lib.QueueElement;

/**
 * @author yuhangzhong
 *
 */
public class PersistentQueue<E> {
	QueueElement<E> head;
	QueueElement<E> tail;
	int queSize;
	
	public PersistentQueue()
	{
		head = null;
		tail = null;
		queSize = 0;
	}
	
	private PersistentQueue(QueueElement<E> h, QueueElement<E> t, int qs)
	{
		head = h;
		tail = t;
		queSize = qs;
	}
	
	//time: O (1)
	public PersistentQueue<E> enqueue(E e)
	{
		if (e == null)
		{
			throw new IllegalArgumentException();
		}
		
		QueueElement<E> tmpHead = head;
		QueueElement<E> tmpTail = new QueueElement<E>();
		tmpTail.curValue = e;
		tmpTail.preElem = tail;
		if (size() == 0)
		{
			tmpHead = tmpTail;
		}
		
		return new PersistentQueue<E>(tmpHead, tmpTail, queSize+1);
	}
	
	//time: O(n)
	public PersistentQueue<E> dequeue()
	{
		if (size() == 0)
		{
			throw new NoSuchElementException();
		}
		
		if (size() == 1)
		{
			return new PersistentQueue<E>(null, null, queSize-1);
		}
		
		QueueElement<E> tmpHead = tail;
		QueueElement<E> tmpTail = tail;
		while (tmpHead.preElem != head)
		{
			tmpHead = tmpHead.preElem;
		}

		return new PersistentQueue<E>(tmpHead, tmpTail, queSize-1);
	}
	
	//O(1)
	public E peek()
	{
		if (size() == 0)
		{
			throw new NoSuchElementException();
		}
		
		return head.curValue;
	}
	
	//O(1)
	public int size()
	{
		return queSize;
	}
}
