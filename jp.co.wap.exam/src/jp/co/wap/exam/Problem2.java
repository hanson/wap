/**
 * 
 */
package jp.co.wap.exam;

import java.util.LinkedList;
import java.util.List;
import jp.co.wap.exam.lib.Interval;

/**
 * @author yuhangzhong
 *
 * Time: O(n+1440)
 * Space: O(n)
 * 
 * T is the time, scope is [0, 1440].
 * dp[T] = max{dp[T-1], dp[inv_k_BeginMinute-1] + inv_k_InvMinute, dp[T]}, 
 * inv_k is those Interval that end at time T.
 * ans = dp[1440]
 * 
 * init:
 * dp[0] = ... = dp[1440] = 0;
 */

public class Problem2 {
	private int[] dp;
	List< List<Interval>> invList;
	
	public int getMaxWorkingTime(List<Interval> intervals)
	{
		int ret = 0;
		if (intervals == null || intervals.size() == 0)
		{
			return ret;
		}
		
		dp = new int[1445];
		invList = new LinkedList< List< Interval >>();
		for (int i = 0; i <= 1440; ++i)
		{
			invList.add(new LinkedList< Interval >());
			dp[i] = 0;
		}
		
		for (int k = 0; k < intervals.size(); ++k)
		{
			Interval cur = intervals.get(k);
			List< Interval > curList = invList.get(cur.getEndMinuteUnit());
			curList.add(intervals.get(k));
		}
		
		for (int outter = 1; outter <= 1440; ++outter)
		{
			dp[outter] = dp[outter - 1];
			List< Interval > curList = invList.get(outter);
			for (int k = 0; k < curList.size(); ++k)
			{
				Interval cur = curList.get(k);
				if (dp[outter] < cur.getIntervalMinute())
				{
					dp[outter] = cur.getIntervalMinute();	
				}
				int inner = cur.getBeginMinuteUnit() - 1;
				if (inner < 0)
				{
					inner = 0;
				}
				
				int tmp = dp[inner] + cur.getIntervalMinute();
				if (dp[outter] < tmp)
				{
					dp[outter] = tmp;	
				}
			}
		}

		return dp[ 1440 ];
	}
}
