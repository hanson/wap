package jp.co.wap.exam.test;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;
import jp.co.wap.exam.*;
import jp.co.wap.exam.lib.Interval;
import junit.framework.Assert;

public class testProblem1 {

	@Test
	public void test() {
		Interval it1 = new Interval("00:00", "02:00");
		Interval it2 = new Interval("02:00", "03:00");
		Interval it3 = new Interval("04:00", "05:00");
		List<Interval> intervals =  new LinkedList<Interval>();
		intervals.add(it1);
		intervals.add(it2);
		intervals.add(it3);
		Problem1 p1 = new Problem1();
		int ret = p1.getMaxIntervalOverlapCount(intervals);
		Assert.assertEquals(ret, 2);
	}

	@Test
	public void test1() {
		Interval it1 = new Interval("08:00", "12:00");
		Interval it2 = new Interval("06:00", "09:00");
		Interval it3 = new Interval("11:00", "13:30");
		List<Interval> intervals =  new LinkedList<Interval>();
		intervals.add(it1);
		intervals.add(it2);
		intervals.add(it3);
		Problem1 p1 = new Problem1();
		int ret = p1.getMaxIntervalOverlapCount(intervals);
		Assert.assertEquals(ret, 2);
	}

	@Test
	public void test2() {
		Interval it1 = new Interval("09:00", "12:30");
		Interval it2 = new Interval("12:00", "14:30");
		Interval it3 = new Interval("11:00", "13:30");
		List<Interval> intervals =  new LinkedList<Interval>();
		intervals.add(it1);
		intervals.add(it2);
		intervals.add(it3);
		Problem1 p1 = new Problem1();
		int ret = p1.getMaxIntervalOverlapCount(intervals);
		Assert.assertEquals(ret, 3);
	}

}
