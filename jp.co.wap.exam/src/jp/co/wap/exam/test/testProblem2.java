package jp.co.wap.exam.test;

import java.util.LinkedList;
import java.util.List;

import jp.co.wap.exam.Problem2;
import jp.co.wap.exam.lib.Interval;

import org.junit.Test;
import junit.framework.Assert;

public class testProblem2 {

	@Test
	public void test() {
		Interval it1 = new Interval("00:00", "02:00");
		Interval it2 = new Interval("02:00", "03:00");
		Interval it3 = new Interval("04:00", "05:00");
		List<Interval> intervals = new LinkedList<Interval>();
		intervals.add(it1);
		intervals.add(it2);
		intervals.add(it3);
		Problem2 p2 = new Problem2();
		int ret = p2.getMaxWorkingTime(intervals);
		Assert.assertEquals(ret, 180);
	}
	
	@Test
	public void test1() {
		Interval it1 = new Interval("06:00", "08:30");
		Interval it2 = new Interval("09:00", "11:30");
		Interval it3 = new Interval("12:30", "14:00");
		List<Interval> intervals = new LinkedList<Interval>();
		intervals.add(it1);
		intervals.add(it2);
		intervals.add(it3);
		Problem2 p2 = new Problem2();
		int ret = p2.getMaxWorkingTime(intervals);
		Assert.assertEquals(ret, 390);
	}
}
