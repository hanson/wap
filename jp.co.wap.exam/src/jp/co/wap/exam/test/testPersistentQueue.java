package jp.co.wap.exam.test;

import jp.co.wap.exam.PersistentQueue;
import junit.framework.Assert;

import org.junit.Test;

public class testPersistentQueue {

	@Test
	public void test() {
		PersistentQueue<String> pq = new PersistentQueue<String>();
		Assert.assertEquals(pq.size(), 0);

		PersistentQueue<String> pq1 = pq.enqueue("a");
		Assert.assertEquals(pq1.size(), 1);
		Assert.assertEquals(pq1.peek(), "a");

		PersistentQueue<String> pq2 = pq1.enqueue("b");
		Assert.assertEquals(pq2.size(), 2);
		Assert.assertEquals(pq2.peek(), "a");

		PersistentQueue<String> pq3 = pq2.enqueue("c");
		Assert.assertEquals(pq3.size(), 3);
		Assert.assertEquals(pq3.peek(), "a");

		PersistentQueue<String> pq4 = pq3.dequeue();
		Assert.assertEquals(pq4.size(), 2);
		Assert.assertEquals(pq4.peek(), "b");

		PersistentQueue<String> pq5 = pq4.dequeue();
		Assert.assertEquals(pq5.size(), 1);
		Assert.assertEquals(pq5.peek(), "c");

		PersistentQueue<String> pq6 = pq5.dequeue();
		Assert.assertEquals(pq6.size(), 0);
		
		//
		Assert.assertEquals(pq.size(), 0);
		Assert.assertEquals(pq1.size(), 1);
		Assert.assertEquals(pq2.size(), 2);
		Assert.assertEquals(pq3.size(), 3);
		Assert.assertEquals(pq4.size(), 2);
		Assert.assertEquals(pq5.size(), 1);
		Assert.assertEquals(pq6.size(), 0);
	}

}
