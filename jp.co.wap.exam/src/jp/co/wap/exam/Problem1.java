package jp.co.wap.exam;

import java.util.List;
import jp.co.wap.exam.lib.Interval;
import jp.co.wap.exam.lib.SegmentTree;
/**
 * @author yuhangzhong
 * 
 *  I implement two solution, 
 *  one is O(n), 
 *  the other is make use of segment, O(n*log5000).
 */
public class Problem1 {

	/**
	 *  time: O(n)
	 *  space: O(1440)
	 */
	public int getMaxIntervalOverlapCount(List<Interval> intervals)
	{
		int ret = 0;
		if (intervals == null || intervals.size() == 0)
		{
			return ret;
		}
		int[] recL = new int[1445];
		int[] recR = new int[1445];

		for (int i = 0; i <= 1440; ++i)
		{
			recL[i] = recR[i] = 0;
		}
		
		for (int index = 0; index < intervals.size(); ++index)
		{
			++recL[ intervals.get(index).getBeginMinuteUnit() ];
			++recR[ intervals.get(index).getEndMinuteUnit() ];
		}
		
		int tmp = 0;
		for (int i = 0; i <= 1440; ++i)
		{
			while (recL[i] > 0)
			{
				--recL[i];
				++tmp;
			}
			if (tmp > ret)
			{
				ret = tmp;
			}
			
			while (recR[i] > 0)
			{
				--recR[i];
				--tmp;
			}
		}
		
		return ret;
	}
	
	/**
	 *	segment tree
	 *  time: O(n*log5000)
	 *  space: O(n)
	 */
	public int getMaxIntervalOverlapCount_v2(List<Interval> intervals)
	{
		int ret = 0;
		if (intervals == null || intervals.size() == 0)
		{
			return ret;
		}

		SegmentTree st = new SegmentTree();
		for (int index = 0; index < intervals.size(); ++index)
		{
			st.insertSeg(intervals.get(index).getBeginMinuteUnit(), intervals.get(index).getEndMinuteUnit());
		}
		ret = st.getMaxIntervalOverlapCount();
		
		return ret;
	}
}

