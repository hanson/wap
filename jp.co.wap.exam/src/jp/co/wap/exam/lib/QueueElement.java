/**
 * 
 */
package jp.co.wap.exam.lib;

/**
 * @author yuhangzhong
 *
 */
public class QueueElement<E> {
	public QueueElement<E> preElem;
	public E curValue;
}
