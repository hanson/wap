/**
 * 
 */
package jp.co.wap.exam.lib;

/**
 * @author yuhangzhong
 *
 */
public class TreeNode {
	private int left;
	private int right;
	private int cnt;//count of segment occurs.
	
	public TreeNode(int left, int right)
	{
		this.left = left;
		this.right = right;
		this.cnt = 0;
	}
	
	public void setCnt(int value)
	{
		this.cnt = value;
	}
	
	public int getCnt()
	{
		return this.cnt;
	}
	
	public void setLeft(int value)
	{
		this.left = value;
	}
	
	public int getLeft()
	{
		return this.left;
	}
	
	public void setRight(int value)
	{
		this.right = value;
	}
	
	public int getRight()
	{
		return this.right;
	}
}
