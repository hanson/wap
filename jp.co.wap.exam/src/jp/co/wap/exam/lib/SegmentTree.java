/**
 * http://en.wikipedia.org/wiki/Segment_tree
 */
package jp.co.wap.exam.lib;

/**
 * @author yuhangzhong
 * biggest minute unit is 24:00 -> 24*60+0 = 1440
 * smallest minute unit is 0
 * so the value is in interval [0, 1440], so count of the tree leaf is 1441,
 * the biggest node index is 4089
 * 
 * this is the tree:
 *      root[left, right, cnt]
 *             [0, 1440,0]
 *               /  \
 *         [0,720,0] [721,1440,0]
 *             .........
 *           .............
 *          ...............\
 *        ..........[1440,1440,0]
 *        /    \  
 *   [0,0,0] [1,1,0]
 */

public class SegmentTree {
	private TreeNode[] segTree;
	private int size;
	private int treeSize;
	private int maxIntervalOverlapCount;
	
	public SegmentTree()
	{
		this.treeSize = -1;
		this.size = 5000;
		this.segTree = new TreeNode[this.size];
		for (int index = 0; index < this.size; ++index)
		{
			this.segTree[index] = new TreeNode(-1, -1);
		}
		
		//buildTree(0, 0, 2);
		buildTree(0, 0, 1440);
		//total tree nodes
		this.treeSize += 1;
		maxIntervalOverlapCount = 0;
		//System.out.println("tree size is: "+this.treeSize);
	}
	
	private void buildTree(int curIndex, int left, int right)
	{
		this.segTree[curIndex].setCnt(0);
		this.segTree[curIndex].setLeft(left);
		this.segTree[curIndex].setRight(right); 
		//System.out.println("tree size is: "+curIndex+" left: "+left+" right:"+right);
		//record the biggest node index to cal the tree size
		if (curIndex > this.treeSize)
		{
			this.treeSize = curIndex;
		}
		
		if (left == right)
		{
			return;
		}
		int mid = (left + right) >> 1;
		buildTree(curIndex*2+1, left, mid);
		buildTree(curIndex*2+2, mid+1, right);
	}
	
	private void updateOverlapCount(int curIndex)
	{
		int curLeft = this.segTree[curIndex].getLeft();
		int curRight = this.segTree[curIndex].getRight();
		this.segTree[curIndex].setCnt( this.segTree[curIndex].getCnt() + 1 );
		if (this.segTree[curIndex].getCnt() > this.maxIntervalOverlapCount)
		{
			this.maxIntervalOverlapCount = this.segTree[curIndex].getCnt();
		}
		
		if (curLeft == curRight)
		{
			return;
		}
		updateOverlapCount(curIndex*2+1);
		updateOverlapCount(curIndex*2+2);
	}
	
	private void insert(int curIndex, int left, int right)
	{
		int curLeft = this.segTree[curIndex].getLeft();
		int curRight = this.segTree[curIndex].getRight();
		if (curLeft == left &&  curRight== right)
		{
			updateOverlapCount(curIndex);
			return;
		}
		
		if (curLeft == curRight)
		{
			return;
		}
		
		int mid = (curLeft + curRight) >> 1;
		if (mid >= right)
		{
			insert(curIndex*2+1, left, right);
		}
		else if (mid < left)
		{
			insert(curIndex*2+2, left, right);
		}
		else
		{
			insert(curIndex*2+1, left, mid);
			insert(curIndex*2+2, mid+1, right);
		}
	}
	
	public void insertSeg(int left, int right)
	{
		insert(0, left, right);
	}
	
	public int getMaxIntervalOverlapCount()
	{
		return maxIntervalOverlapCount;
	}
}

